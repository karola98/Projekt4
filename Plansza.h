#pragma once

enum class Plansza
{
	none = '_',
	human = 'O',
	computer = 'X'
};