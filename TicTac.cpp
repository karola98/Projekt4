#include "pch.h"
#include "TicTac.h"


/*____________________________________________________________________________________________________________________________________________________________*/

TicTac::TicTac(int s, int z)
{
	size = s;
	znaki = z;
	board = new Plansza*[size];
	for (int i = 0; i < size; i++)
		board[i] = new Plansza[size];


	max_depth = ceil(log2(size));
}

TicTac::~TicTac()
{
		delete [] board;
}

void TicTac::inicjacja()
{
	for (int i = 0; i < size; i++)
	{
		for (unsigned int j = 0; j < size; j++)
		{
			board[i][j] = Plansza::none;
		}
	}
}

void TicTac::printBoard()
{
	std::cout << "\n";
	for (int i = 0; i < size; i++)
		std::cout << "-------";

	for (unsigned int i = 0; i < size; i++)
	{
		std::cout << "\n|";
		for (unsigned int j = 0; j < size; j++)
		{
			std::cout << std::setw(3) << static_cast<char>(board[i][j]) << std::setw(3) << " |";
		}
	}
	
	std::cout << "\n";
	for (int i = 0; i < size; i++)
		std::cout << "-------";
	std::cout << "\n";
}

/*____________________________________________________________________________________________________________________________________________________________*/

int TicTac::isValid(Move m) {

	if ((m.row > (size - 1)) || (m.row < 0) ||
		(m.col > (size - 1)) || (m.col < 0))
		return 0;

	if (board[m.row][m.col] == Plansza::none)
		return 1;
	else
		return 0;
}

bool TicTac::isTerminal()
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (board[i][j] == Plansza::none)
				return false;
		}
	}
	return true;
}

bool TicTac::rowCrossed()
{
	int countO, countX;
	for (int i = 0; i < size; i++)
	{
		int j = 0;
		while (j < size)
		{
			countO = 0;
			countX = 0;
			int l = j;
			if (l + znaki <= size)
			{
				for (int k = 0; k < znaki; k++)
				{
					if (board[i][l] == Plansza::computer)
						countX++;
					else if (board[i][l] == Plansza::human)
						countO++;
					l++;
				}
			}

			if (countO == 0)
			{
				if (countX == znaki)
					return true;
			}
			if (countX == 0)
			{
				if (countO == znaki)
					return true;
			}

			j++;
		}
	}
	return false;
}

bool TicTac::columnCrossed()
{
	int countO, countX;
	for (int i = 0; i < size; i++)
	{
		int j = 0;
		while (j < size)
		{
			countO = 0;
			countX = 0;
			int l = j;
			if (l + znaki <= size)
			{
				for (int k = 0; k < znaki; k++)
				{
					if (board[l][i] == Plansza::computer)
						countX++;
					else if (board[l][i] == Plansza::human)
						countO++;
					l++;
				}
			}

			if (countO == 0)
			{
				if (countX == znaki)
					return true;
			}
			if (countX == 0)
			{
				if (countO == znaki)
					return true;
			}

			j++;
		}
	}

	return false;
}

bool TicTac::diagonalCrossed()
{
	int countO, countX, countOP, countXP;
	/* PRZEKĄTNA W PRAWO _______________________________________________________________________________________*/
	int j = 0;
	while (j < size)
	{
		countO = 0;
		countX = 0;
		int l = j;
		if (l + znaki <= size)
		{
			for (int k = 0; k < znaki; k++)
			{
				if (board[l][l] == Plansza::computer)
					countX++;
				else if (board[l][l] == Plansza::human)
					countO++;
				l++;
			}
		}

		if (countO == 0)
		{
			if (countX == znaki)
				return true;
		}
		if (countX == 0)
		{
			if (countO == znaki)
				return true;
		}
		j++;
	}

	/* PONIŻEJ I POWYŻEJ PRZEKĄTNEJ _________________________________________________________________________________*/
	for (int i = 1; i < size; i++)
	{
		j = 0;
		int p = i;
		while (j < size && p < size)
		{
			countO = 0;
			countOP = 0;
			countX = 0;
			countXP = 0;
			int l = j;
			int m = p;
			if ((l + znaki) <= size && (m + znaki) <= size)
			{
				for (int k = 0; k < znaki; k++)
				{
					if (board[m][l] == Plansza::computer)
						countX++;
					else if (board[m][l] == Plansza::human)
						countO++;

					if (board[l][m] == Plansza::computer)
						countXP++;
					else if (board[l][m] == Plansza::human)
						countOP++;
					l++;
					m++;
				}
			}

			if (countO == 0 || countOP == 0)
			{
				if (countX == znaki)
					return true;
				if (countXP == znaki)
					return true;
			}
			if (countX == 0 || countXP)
			{
				if (countO == znaki)
					return true;
				if (countOP == znaki)
					return true;
			}

			j++;
			p++;
		}
	}

	/* PRZEKĄTNA W LEWO _____________________________________________________________________________________________*/
	int i = 0;
	j = size - 1;
	while (j >= 0 && i < size)
	{
		countO = 0;
		countX = 0;
		int l = j;
		int m = i;
		if ((l - znaki) >= -1 && (m <= size))
		{
			for (int k = 0; k < znaki; k++)
			{
				if (board[m][l] == Plansza::computer)
					countX++;
				else if (board[m][l] == Plansza::human)
					countO++;
				l--;
				m++;
			}
		}

		if (countO == 0)
		{
			if (countX == znaki)
				return true;
		}
		if (countX == 0)
		{
			if (countO == znaki)
				return true;
		}

		j--;
		i++;
	}

	// POWYŻEJ PRZEKĄTNEJ
	for (int i = size - 2; i >= 0; i--)
	{
		j = 0;
		int p = i;
		while (j < size && p >= 0)
		{
			countO = 0;
			countX = 0;
			int l = j;
			int m = p;
			if ((l + znaki) <= size && (m - znaki) >= -1)
			{
				for (int k = 0; k < znaki; k++)
				{
					if (board[l][m] == Plansza::computer)
						countX++;
					else if (board[l][m] == Plansza::human)
						countO++;
					l++;
					m--;
				}
			}

			if (countO == 0)
			{
				if (countX == znaki)
					return true;
			}
			if (countX == 0)
			{
				if (countO == znaki)
					return true;

			}

			j++;
			p--;
		}
	}

	// PONIŻEJ PRZEKĄTNEJ
	for (int i = 1; i < size; i++)
	{
		j = size - 1;
		int p = i;
		while (j >= 0 && p < size)
		{
			countO = 0;
			countX = 0;
			int l = j;
			int m = p;
			if ((l - znaki) >= 0 && (m + znaki) <= size)
			{
				for (int k = 0; k < znaki; k++)
				{
					if (board[l][m] == Plansza::computer)
						countX++;
					else if (board[l][m] == Plansza::human)
						countO++;
					l--;
					m++;
				}
			}

			if (countO == 0)
			{
				if (countX == znaki)
					return true;
			}
			if (countX == 0)
			{
				if (countO == znaki)
					return true;
			}

			j--;
			p++;
		}
	}

	return false;
}

bool TicTac::checkWin()
{
	// RZĘDY
	if (rowCrossed())
		return true;

	// KOLUMNY
	if (columnCrossed())
		return true;

	// PRZEKĄTNE
	if (diagonalCrossed())
		return true;

	return false;
}

/*____________________________________________________________________________________________________________________________________________________________*/

int TicTac::Ewaluacja()
{
	std::vector<int> plusy;
	std::vector<int> minusy;
	int countX, countO, countB, countXP, countOP;

	// Sprawdzanie poziomo każdego z rzędów _______________________________________________________________
	for (int i = 0; i < size; i++)
	{
		int j = 0;
		while (j < size)
		{
			countO = 0;
			countX = 0;
			int l = j;
			if (l + znaki <= size)
			{
				for (int k = 0; k < znaki; k++)
				{
					if (board[i][l] == Plansza::computer)
						countX++;
					else if (board[i][l] == Plansza::human)
						countO++;
					l++;
				}
			}

			if (countO == 0)
			{
				if (countX == znaki)
					return pow(10, znaki);
				if (countX != 0)
					plusy.push_back(pow(10, countX));
			}
			if (countX == 0)
			{
				if (countO == znaki)
					return -pow(10, znaki);

				if (countO != 0)
					minusy.push_back(-pow(10, countO));
			}

			j++;
		}
	}


	/* KOLUMNY ______________________________________________________________________________________________________________________________*/
	for (int i = 0; i < size; i++)
	{
		int j = 0;
		while (j < size)
		{
			countO = 0;
			countX = 0;
			int l = j;
			if (l + znaki <= size)
			{
				for (int k = 0; k < znaki; k++)
				{
					if (board[l][i] == Plansza::computer)
						countX++;
					else if (board[l][i] == Plansza::human)
						countO++;
					l++;
				}
			}

			if (countO == 0)
			{
				if (countX == znaki)
					return pow(10, znaki);
				if (countX != 0)
					plusy.push_back(pow(10, countX));
			}
			if (countX == 0)
			{
				if (countO == znaki)
					return -pow(10, znaki);

				if (countO != 0)
					minusy.push_back(-pow(10, countO));
			}

			j++;
		}
	}

	/* PRZEKĄTNA W PRAWO ____________________________________________________________________________________________________________________*/
	int j = 0;
	while (j < size)
	{
		countO = 0;
		countX = 0;
		int l = j;
		if (l + znaki <= size)
		{
			for (int k = 0; k < znaki; k++)
			{
				if (board[l][l] == Plansza::computer)
					countX++;
				else if (board[l][l] == Plansza::human)
					countO++;
				l++;
			}
		}

		if (countO == 0)
		{
			if (countX == znaki)
				return pow(10, znaki);
			if (countX != 0)
				plusy.push_back(pow(10, countX));
		}
		if (countX == 0)
		{
			if (countO == znaki)
				return -pow(10, znaki);

			if (countO != 0)
				minusy.push_back(-pow(10, countO));
		}

		j++;
	}

	/* PONIŻEJ i POWYŻEj PRZEKĄTNEJ */

	for (int i = 1; i < size; i++)
	{
		j = 0;
		int p = i;
		while (j < size && p < size)
		{
			countO = 0;
			countOP = 0;
			countX = 0;
			countXP = 0;
			int l = j;
			int m = p;
			if ((l + znaki) <= size && (m + znaki) <= size)
			{
				for (int k = 0; k < znaki; k++)
				{
					if (board[m][l] == Plansza::computer)
						countX++;
					else if (board[m][l] == Plansza::human)
						countO++;

					if (board[l][m] == Plansza::computer)
						countXP++;
					else if (board[l][m] == Plansza::human)
						countOP++;
					l++;
					m++;
				}
			}

			if (countO == 0 || countOP == 0)
			{
				if (countX == znaki)
					return pow(10, znaki);
				if (countX != 0)
					plusy.push_back(pow(10, countX));
				if (countXP == znaki)
					return pow(10, znaki);
				if (countXP != 0)
					plusy.push_back(pow(10, countXP));
			}
			if (countX == 0 || countXP)
			{
				if (countO == znaki)
					return -pow(10, znaki);

				if (countO != 0)
					minusy.push_back(-pow(10, countO));

				if (countOP == znaki)
					return -pow(10, znaki);

				if (countOP != 0)
					minusy.push_back(-pow(10, countOP));
			}

			j++;
			p++;
		}
	}

	/* PRZEKĄTNA W LEWO __________________________________________________________*/

	int i = 0;
	j = size - 1;
	while (j >= 0 && i < size)
	{
		countO = 0;
		countX = 0;
		int l = j;
		int m = i;
		if ((l - znaki) >= -1 && (m <= size))
		{
			for (int k = 0; k < znaki; k++)
			{
				if (board[m][l] == Plansza::computer)
					countX++;
				else if (board[m][l] == Plansza::human)
					countO++;
				l--;
				m++;
			}
		}

		if (countO == 0)
		{
			if (countX == znaki)
				return pow(10, znaki);
			if (countX != 0)
				plusy.push_back(pow(10, countX));
		}
		if (countX == 0)
		{
			if (countO == znaki)
				return -pow(10, znaki);

			if (countO != 0)
				minusy.push_back(-pow(10, countO));
		}

		j--;
		i++;
	}

	// powyżej przekątnej
	for (int i = size - 2; i >= 0; i--)
	{
		j = 0;
		int p = i;
		while (j < size && p >= 0)
		{
			countO = 0;
			countX = 0;
			int l = j;
			int m = p;
			if ((l + znaki) <= size && (m - znaki) >= -1)
			{
				for (int k = 0; k < znaki; k++)
				{
					if (board[l][m] == Plansza::computer)
						countX++;
					else if (board[l][m] == Plansza::human)
						countO++;
					l++;
					m--;
				}
			}

			if (countO == 0)
			{
				if (countX == znaki)
					return pow(10, znaki);
				if (countX != 0)
					plusy.push_back(pow(10, countX));
			}
			if (countX == 0)
			{
				if (countO == znaki)
					return -pow(10, znaki);

				if (countO != 0)
					minusy.push_back(-pow(10, countO));
			}

			j++;
			p--;
		}
	}
	// poniżej przekątnej
	for (int i = 1; i < size; i++)
	{
		j = size - 1;
		int p = i;
		while (j >= 0 && p < size)
		{
			countO = 0;
			countX = 0;
			int l = j;
			int m = p;
			if ((l - znaki) >= 0 && (m + znaki) <= size)
			{
				for (int k = 0; k < znaki; k++)
				{
					if (board[l][m] == Plansza::computer)
						countX++;
					else if (board[l][m] == Plansza::human)
						countO++;
					l--;
					m++;
				}
			}

			if (countO == 0)
			{
				if (countX == znaki)
					return pow(10, znaki);
				if (countX != 0)
					plusy.push_back(pow(10, countX));
			}
			if (countX == 0)
			{
				if (countO == znaki)
					return -pow(10, znaki);

				if (countO != 0)
					minusy.push_back(-pow(10, countO));
			}

			j--;
			p++;
		}
	}

	int min, max;
	if (!minusy.empty())
		min = *(std::min_element(minusy.begin(), minusy.end()));
	if (!plusy.empty())
		max = *(std::max_element(plusy.begin(), plusy.end()));

	if (minusy.empty() && plusy.empty())
		return 0;
	else if (minusy.empty())
		return max;
	else if (plusy.empty())
		return min;
	else if (abs(min) > max)
		return min;
	else if (max >= abs(min))
		return max;

	return 0;
}

Move TicTac::minimax(int d, int alfa, int beta, bool isMax)
{
	Move found, bestmove;
	Move temp;
	int i, j;


	// Jeśli osiągnięto maksymalną głębokość lub nie ma więcej ruchów przeprowadź ewaluację
	if ((d == max_depth) || (isTerminal())) {
		found.value = Ewaluacja();

		return found;
	}

	if (isMax)
	{
		// Jak na razie najlepszym możliwym ruchem jest alfa
		bestmove.value = alfa;

		// Przechodzimy przez wszystkie pola
		for (i = 0; i < size; i++)
			for (j = 0; j < size; j++) {
				temp.row = i;
				temp.col = j;
				// Jeśli pole jest wolne 
				if (isValid(temp)) {

					// Wykonujemy ruch
					makeMove(temp, Plansza::computer);
					// Sprawdzamy jak dobry jest nasz ruch
					found = minimax(d + 1, bestmove.value, beta, false);
					undo(temp);
					// Jeśli jest lepszy niż nasz najlepszy ruch do tej pory
					if (found.value > bestmove.value) {
						bestmove.value = found.value;
						bestmove.row = i;
						bestmove.col = j;
						// Jeśli jest lepszy niż beta - odcinamy
						if (found.value > beta) {
							bestmove.value = beta;
							return bestmove;
						}
					}
				}
			}
		return bestmove;
	}

	else
	{
		bestmove.value = beta;

		for (i = 0; i < 3; i++)
			for (j = 0; j < 3; j++) {
				temp.row = i;
				temp.col = j;
				if (isValid(temp)) {
					makeMove(temp, Plansza::human);
					found = minimax(d + 1, alfa, bestmove.value, true);
					undo(temp);
					if (found.value < bestmove.value) {
						bestmove.value = found.value;
						bestmove.row = i;
						bestmove.col = j;
						if (found.value < alfa) {
							bestmove.value = alfa;
							return bestmove;
						}
					}
				}
			}

		return bestmove;
	}

}

/*Move TicTac::max(int d, int alfa, int beta){
	Move m, bestmove;
	Move temp;
	int i, j;
	int max_depth = ceil(log2(size));

	if ((d == max_depth) || (isTerminal())) {
		m.value = Ewaluacja();

		return m;
	}

	bestmove.value = alfa;

	for (i = 0; i < size; i++)
		for (j = 0; j < size; j++) {
			temp.row = i;
			temp.col = j;
			if (isValid(temp)) {
				makeMove(temp, Plansza::computer
);
				m = min(d + 1, bestmove.value, beta);
				undo(temp);
				if (m.value > bestmove.value) {
					bestmove.value = m.value;
					bestmove.row = i;
					bestmove.col = j;
					if (m.value > beta) {
						bestmove.value = beta;
						return bestmove;
					}
				}
			}
		}

	return bestmove;
}

Move TicTac::min(int d, int alfa, int beta) {
	Move m, bestmove;
	Move temp;
	int i, j;

	int max_depth = ceil(log2(size));
	if ((d == max_depth) || (isTerminal())) {
		m.value = Ewaluacja();
		return m;
	}

	bestmove.value = beta;

	for (i = 0; i < 3; i++)
		for (j = 0; j < 3; j++) {
			temp.row = i;
			temp.col = j;
			if (isValid(temp)) {
				makeMove(temp, Plansza::human);
				m = max(d + 1, alfa, bestmove.value);
				undo(temp);
				if (m.value < bestmove.value) {
					bestmove.value = m.value;
					bestmove.row = i;
					bestmove.col = j;
					if (m.value < alfa) {
						bestmove.value = alfa;
						return bestmove;
					}
				}
			}
		}

	return bestmove;
}*/

/*____________________________________________________________________________________________________________________________________________________________*/
bool TicTac::makeMove(Move oper, Plansza who) {
	if (!isValid(oper))
		return 0;

	board[oper.row][oper.col] = who;

	return 1;
}

void TicTac::undo(Move oper) {
	if ((oper.row <= (size - 1)) || (oper.row >= 0) ||
		(oper.col <= (size - 1)) || (oper.col >= 0))
		board[oper.row][oper.col] = Plansza::none;
}

void TicTac::getHumanMove()
{
	Move oper;

	do {
		std::cout << "Twoj ruch \n";

		std::cout << "Rzad (1-" << size << "): ";
		std::cin >> oper.row;

		std::cout << "Kolumna (1-" << size << "): ";
		std::cin >> oper.col;

		oper.row--;
		oper.col--;
	} while (!isValid(oper));

	makeMove(oper, Plansza::human);
}

void TicTac::play()
{
	unsigned int turn = 0;
	bool exit = false;

	printBoard();
	do
	{
		// human move
		if (turn == 0)
		{
			getHumanMove();

			if (checkWin())
			{
				std::cout << "Wygrales!\n";
				exit = true;
			}
		}
		else
		{
			std::cout << "\nKolej komputera: ";

			Move aimove = minimax(0, INT_MIN, INT_MAX, true);
	
			makeMove(aimove, Plansza::computer);

			if (checkWin())
			{
				std::cout << "Komputer wygrywa\n";
				exit = true;
			}
		}

		if (isTerminal())
		{
			std::cout << "\n*** Remis ***\n";
			exit = true;
		}

		turn ^= 1;
		printBoard();

	} while (!exit);
}