#pragma once
#include "Plansza.h"
#include "Move.h"
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <limits>
#include <vector>
#include <iterator>

class TicTac
{

	Plansza **board;
	int size;
	int znaki;
public:
	TicTac(int s, int z);
	~TicTac();
	// PLANSZA
	void inicjacja();
	void printBoard();

	// SPRAWDZANIE
	int isValid(Move m);
	bool isTerminal();
	bool rowCrossed();
	bool columnCrossed();
	bool diagonalCrossed();
	bool checkWin();

	// ALGORYTMY
	int Ewaluacja();
	Move max(int d, int alpha, int beta);
	Move min(int d, int alpha, int beta);

	// GRA
	bool makeMove(Move oper, Plansza who);
	void undo(Move oper);
	void getHumanMove();
	void play();
};

